import pandas as pd
import matplotlib.pyplot as plt

def to_float(snum):
    return float(snum.replace('.', '').replace(',', '.'))

def main(records_path):
    column_names = ['Fecha', 'Tipo', 'Especie', 'Cantidad', 'Precio', 'Monto Neto', 'Moneda', 'Estado']
    records = pd.read_excel(records_path, header=2, names=column_names, index_col=len(column_names))  # type: pd.DataFrame
    records['Cantidad'] = records['Cantidad'].apply(to_float)
    records['Fecha'] = records['Fecha'].apply(pd.to_datetime)

    fig, ax = plt.subplots()
    for key, grp in records.groupby(['Especie']):
        ax = grp.plot(ax=ax, kind='line', x='Fecha', y='Cantidad', label=key)
    
    print(records)
    plt.legend(loc='best')
    plt.show()

if __name__ == '__main__':
    main('./movimientos.xls')
